#!/usr/bin/python3

import socket
import signal
import sys
from cryptography.fernet import Fernet

def signal_handler(sig, frame):
    print('\rAbort, exit application...')
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

s = socket.socket()
host = input(str("Please enter the address of sender : "))
port = 8080
s.connect((host, port))
print("Connected ... ")

filename = input(str("filename dest : "))
file = open(filename, 'wb')
file_data = s.recv(1024)

while (file_data): 
    file.write(file_data)
    file_data = s.recv(1024)

file.close()
print("file received")
