#!/usr/bin/python3

import socket
import signal
import sys
from cryptography.fernet import Fernet

def encrypt_packet():
    key = Fernet.generate_key()
    print(key)
    f = Fernet(key)
    token = f.encrypt(b"my deep dark secret")
    print(token)
    res = f.decrypt(token)
    print(res.decode("utf-8"))


def signal_handler(sig, frame):
    print('\rAbort, exit application...')
    s.close()
    sys.exit(0)

signal.signal(signal.SIGINT, signal_handler)

s = socket.socket()
host = socket.gethostname()
port = 8080
s.bind(("0.0.0.0", port))
s.listen(1)
print(host)
print("Waiting for any incoming connections ...")
conn, addr = s.accept()
print(addr, "Has connected to the server")

filename = input(str("filename path : "))
file = open(filename, 'rb')
file_data = file.read(1024)

while (file_data):
    conn.send(file_data)
    file_data = file.read(1024)

print("File sent.")

s.close()
