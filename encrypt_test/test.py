#!/usr/bin/python3

from cryptography.fernet import Fernet
key = Fernet.generate_key()
print(key)
f = Fernet(key)
token = f.encrypt(b"my deep dark secret")
print(token)
res = f.decrypt(token)
print(res)#.decode())
